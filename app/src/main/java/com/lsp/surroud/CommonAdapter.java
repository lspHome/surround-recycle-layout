package com.lsp.surroud;

import android.animation.Animator;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Copyright © 2019 zanezhao All rights reserved.
 * Created by zanezhao on 2019-07-09 15:25.
 * @author: zanezhao
 * @version: V1.0
 */
public class CommonAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder>  {

    private OnItemConvertable mConvertable;

    private TranslateAnimation mSelectAnimation = new TranslateAnimation();

    public CommonAdapter(@LayoutRes int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public CommonAdapter(@LayoutRes int layoutResId, OnItemConvertable<T> convertable) {
        super(layoutResId);
        mConvertable=convertable;
    }

    public CommonAdapter(@LayoutRes int layoutResId, @Nullable List<T> data, OnItemConvertable<T> convertable) {
        super(layoutResId,data);
        mConvertable=convertable;
    }

    public CommonAdapter setHeader(View view){
        addHeaderView(view);
        return this;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        if (mConvertable!=null)
        mConvertable.convert(helper,item);
    }


    public interface OnItemConvertable<V>{
        void convert(BaseViewHolder helper, V item);
    }

    @Override
    public void onViewAttachedToWindow(BaseViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        addAnimation(holder);
    }

    private void addAnimation(BaseViewHolder holder) {
        for (Animator anim : mSelectAnimation.getAnimators(holder.itemView)) {
            anim.setDuration(1000).start();
            anim.setInterpolator(new LinearInterpolator());
        }
    }
}
