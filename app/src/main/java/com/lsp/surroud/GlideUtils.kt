package com.lsp.surroud

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class GlideUtils {
    companion object{
        fun displayImage(context: Context?, path: Any?, imageView: ImageView) {
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            Glide.with(context!!).load(path).apply(RequestOptions.centerInsideTransform().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher)).into(imageView)
        }

        fun displayCircleImage(context: Context?, path: Any?, imageView: ImageView) {
            imageView.scaleType = ImageView.ScaleType.FIT_XY
            Glide.with(context!!).load(path).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.ic_launcher)).into(imageView)
        }
    }

}