package com.lsp.surroud

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import com.chad.library.adapter.base.BaseViewHolder
import kotlinx.android.synthetic.main.activity_main.mRecycle
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {
    var list = arrayListOf<Int>()
    var layoutManager: SurroundLayoutManager? = null
    var mAdapter:CommonAdapter<Int>? =null
    var animation = TranslateAnimation()
//    var old:TagImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        for (i in 0 until 30) {
            val imgname = "img" + (i+1)
            val imgid: Int = resources.getIdentifier(imgname, "drawable", "com.lsp.surroud")
            list.add(imgid)
        }
        mRecycle.layoutManager = SurroundLayoutManager(this).also {
            layoutManager = it
        }
        tvClose.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.alpha_in, R.anim.alpha_out)
        }
        val imgname = "img" + (intent.getIntExtra("POS",1))
        val imgid: Int = resources.getIdentifier(imgname, "drawable", "com.lsp.surroud")
        ivSelect.setBackgroundResource(imgid)
        mAdapter = CommonAdapter<Int>(R.layout.item_img, list) { helper: BaseViewHolder, s: Int ->
            var item =   helper.itemView as TagImageView
            item.tagResource = s
            item.setBackgroundResource(s)
            helper.setOnClickListener(R.id.img) {
                list.shuffle();
                mRecycle.adapter?.notifyDataSetChanged()
                ivSelect.setBackgroundResource(s)
                var tag = item.tag as PositionTag
                addAnimation(ivSelect,tag.nowX - ScreenUtil.getScreenWidth(this)/2,tag.nowY - ScreenUtil.getScreenHeight(this)/2)
            }
        }
        mRecycle.adapter = mAdapter
    }

    private fun addAnimation(view: View,transX:Int,tranY:Int) {
        for (anim in animation.getAnimators(view,transX,tranY)) {
            anim.setDuration(1000).start()
            anim.interpolator = LinearInterpolator()
        }
    }

    private fun addAnimation2(view: View,transX:Int,tranY:Int) {
        for (anim in animation.getScaleAnimators(view,transX,tranY)) {
            anim.setDuration(1000).start()
            anim.interpolator = LinearInterpolator()
        }
    }
}