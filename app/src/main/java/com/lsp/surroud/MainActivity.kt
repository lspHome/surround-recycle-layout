package com.lsp.surroud

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.StaggeredGridLayoutManager
import android.widget.ImageView
import com.chad.library.adapter.base.BaseViewHolder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var list = arrayListOf<Int>()
    var mAdapter:CommonAdapter<Int>? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        for (i in 0 until 30) {
            val imgname = "img" + (i+1)
            val imgid: Int = resources.getIdentifier(imgname, "drawable", "com.lsp.surroud")
            list.add(imgid)
        }
        mRecycle.layoutManager = StaggeredGridLayoutManager(8,StaggeredGridLayoutManager.VERTICAL)
        mRecycle.adapter  = CommonAdapter<Int>(R.layout.item_img2, list) { helper: BaseViewHolder, s: Int ->
            helper.getView<ImageView>(R.id.img).setBackgroundResource(s)
            helper.setOnClickListener(R.id.img) {
                var intent = Intent(this,Main2Activity::class.java)
                intent.putExtra("POS",helper.layoutPosition+1)
                startActivity(intent)
               overridePendingTransition(R.anim.alpha_in, R.anim.alpha_out)
            }
        }
    }
}