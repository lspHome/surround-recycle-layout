package com.lsp.surroud;

public class PositionTag {
    public int oldX;
    public int oldY;
    public int nowX;
    public int nowY;

    public PositionTag(int oldX, int oldY, int nowX, int nowY) {
        this.oldX = oldX;
        this.oldY = oldY;
        this.nowX = nowX;
        this.nowY = nowY;
    }

    @Override
    public String toString() {
        return "PositionTag{" +
                "oldX=" + oldX +
                ", oldY=" + oldY +
                ", nowX=" + nowX +
                ", nowY=" + nowY +
                '}';
    }
}
