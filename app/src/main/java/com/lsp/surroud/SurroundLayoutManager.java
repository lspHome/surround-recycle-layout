package com.lsp.surroud;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *author:lsp
 *data:2021/5/15
 *desc:环绕layoutManger
 */

public class SurroundLayoutManager extends RecyclerView.LayoutManager {

    //空白的一半
    public int halfBankWidth = 720;
    public int halfBankHeight = 440;
    
    private static final String TAG = SurroundLayoutManager.class.getSimpleName();
    final SurroundLayoutManager self = this;

    protected int width, height;
    private int left, top, right;
    //最大容器的宽度
    private int usedMaxWidth;
    //竖直方向上的偏移量
    private int verticalScrollOffset = 0;

    private Context mContext = null;

    public int getTotalHeight() {
        return totalHeight;
    }

    //计算显示的内容的高度
    protected int totalHeight = 0;
    private Row row = new Row();
    private List<Row> lineRows = new ArrayList<>();
    private Map<String,PositionTag> oldTag = new HashMap<>();

    //保存所有的Item的上下左右的偏移量信息
    private SparseArray<Rect> allItemFrames = new SparseArray<>();

    public SurroundLayoutManager(Context mContext) {
        this.mContext = mContext;
        setAutoMeasureEnabled(true);
    }

    //每个item的定义
    public class Item {
        int useHeight;
        View view;

        public void setRect(Rect rect) {
            this.rect = rect;
        }

        Rect rect;

        public Item(int useHeight, View view, Rect rect) {
            this.useHeight = useHeight;
            this.view = view;
            this.rect = rect;
        }
    }

    //行信息的定义
    public class Row {
        public void setCuTop(float cuTop) {
            this.cuTop = cuTop;
        }

        public void setMaxHeight(float maxHeight) {
            this.maxHeight = maxHeight;
        }

        //每一行的头部坐标
        float cuTop;
        //每一行需要占据的最大高度
        float maxHeight;
        //每一行存储的item
        List<Item> views = new ArrayList<>();

        public void addViews(Item view) {
            views.add(view);
        }
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    //该方法主要用来获取每一个item在屏幕上占据的位置
    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        Log.d(TAG, "onLayoutChildren");
        totalHeight = 0;
        int cuLineTop = top;
        //当前行使用的宽度
        int cuLineWidth = 0;

        int maxHeightItem = 0;
        row = new Row();
        lineRows.clear();
        allItemFrames.clear();
        removeAllViews();
        if (getItemCount() == 0) {
            detachAndScrapAttachedViews(recycler);
            verticalScrollOffset = 0;
            return;
        }
        if (getChildCount() == 0 && state.isPreLayout()) {
            return;
        }
        //onLayoutChildren方法在RecyclerView 初始化时 会执行两遍
        detachAndScrapAttachedViews(recycler);
        if (getChildCount() == 0) {
            width = getWidth();
            height = getHeight();
            left = getPaddingLeft();
            right = getPaddingRight();
            top = getPaddingTop();
            usedMaxWidth = width - left - right;
        }
        int num = getItemCount();
        for (int i = 0; i < num; i++) {
            Log.d(TAG, "index:" + i);
            View childAt = recycler.getViewForPosition(i);
            if (View.GONE == childAt.getVisibility()) {
                continue;
            }
            measureChildWithMargins(childAt, 0, 0);
            int childWidth = getDecoratedMeasuredWidth(childAt);
            int childHeight = getDecoratedMeasuredHeight(childAt);
            int childUseWidth = childWidth;
            int childUseHeight = childHeight;
//            int minLeft = 0;
//            int maxLeft = 0;
            int minTop = 0;
            int maxTop = 0;
//            switch (i%6){
////                case 0:
////                    maxLeft = usedMaxWidth/2 - childWidth- 240;
////                    maxTop = height/2 - childHeight - 180;
////                    break;
////                case 1:
////                    minLeft = usedMaxWidth/2 + 240;
////                    maxLeft = usedMaxWidth - childWidth;
////                    maxTop = height/2 - childHeight - 180;
////                    break;
////                case 2:
////                    minLeft = usedMaxWidth/2 + 240;
////                    maxLeft = usedMaxWidth - childWidth;
////                    minTop = height/2 + 180;
////                    maxTop = height - childHeight;
////                    break;
////                case 3:
////                    maxLeft = usedMaxWidth/2 - childWidth- 240;
////                    minTop = height/2 + 180;
////                    maxTop = height - childHeight;
////                    break;
//                case 0:
//                case 3:
//                    minLeft = 0;
//                    maxLeft = usedMaxWidth/2 - childWidth- 240;
//                    minTop = 0;
//                    maxTop = height - childHeight;
//                    break;
//                case 1:
//                    minLeft = usedMaxWidth/2 - childWidth- 240;
//                    maxLeft = usedMaxWidth/2 + 240;
//                    minTop = 0;
//                    maxTop = height/2 - childHeight - 180;
//                    break;
//                case 2:
//                case 5:
//                    minLeft = usedMaxWidth/2 + 240;
//                    maxLeft = usedMaxWidth - childWidth;
//                    minTop = 0;
//                    maxTop = height - childHeight;
//                    break;
//                case 4:
//                    minLeft = usedMaxWidth/2 - childWidth- 240;
//                    maxLeft = usedMaxWidth/2 + 240;
//                    minTop =  height/2 +  180;
//                    maxTop = height - childHeight;
//                    break;
//            }
//            minLeft = Math.min(minLeft,maxLeft) ;
//            maxLeft = Math.max(minLeft,maxLeft) ;
//            Log.e(TAG+i, "minLeft: " + minLeft  +  ",maxLeft: " + maxLeft +",minTop: " + minTop+",maxTop: " +maxTop+",childWidth: " +childWidth+",childHeight: " +childHeight );
//            int itemLeft = (int)(minLeft + Math.random()*(maxLeft-minLeft + 1));
            double angle = Math.toRadians(((double) i) / ((double) num) * 360d);
            int width = usedMaxWidth - childWidth;
            int itemLeft = width / 2 + (int) (Math.cos(angle) * (double) width / 2d);
            int itemHeight = height - childHeight;
            int itemCalcTop = itemHeight / 2 + (int) (Math.sin(angle) * (double) itemHeight / 2d);
            if ((itemLeft > usedMaxWidth / 2 - childWidth - halfBankWidth) && (itemLeft < usedMaxWidth / 2 + halfBankWidth)) {
                if (i > getItemCount() / 2) {
                    minTop = height / 2 + halfBankHeight;
                    maxTop = height - childHeight;
                    minTop = Math.max(minTop, itemCalcTop);
                } else {
                    minTop = 0;
                    maxTop = height / 2 - childHeight - halfBankHeight;
                    maxTop = Math.min(itemCalcTop, maxTop);
                }
            } else {
//                minTop = itemCalcTop - 50;
//                maxTop = itemCalcTop + 50;
//                if (i > getItemCount()/2){
//                    minTop = 0;
//                    maxTop = itemCalcTop;
//                }else {
//                    minTop = itemCalcTop;
//                    maxTop = height - childHeight;
//                }
//                minTop = 0;
//                maxTop = height/2 - childHeight;
                if (i > getItemCount()/2){
                    minTop = 0;
                    maxTop = height/2;
                }else {
                    minTop = height/2;
                    maxTop = height - childHeight;
                }

            }
            int itemTop = (int) (minTop + Math.random() * (maxTop - minTop + 1));
            TagImageView img = (TagImageView) childAt;
            PositionTag position = oldTag.get(img.getOnlyTag());
            if (position == null) {
                position = new PositionTag(0, 0, ScreenUtil.getScreenWidth(mContext)/2, ScreenUtil.getScreenHeight(mContext)/2);
            }
            PositionTag nowPos = new PositionTag(position.nowX, position.nowY, itemLeft + childWidth / 2, itemTop + childHeight / 2);
            oldTag.put(img.getOnlyTag(), nowPos);
            childAt.setTag(nowPos);
            Rect frame = allItemFrames.get(i);
            if (frame == null) {
                frame = new Rect();
            }
            frame.set(itemLeft, itemTop, itemLeft + childWidth, itemTop + childHeight);
            allItemFrames.put(i, frame);
            cuLineWidth += childUseWidth;
            maxHeightItem = Math.max(maxHeightItem, childUseHeight);
            row.addViews(new Item(childUseHeight, childAt, frame));

            //如果加上当前的item还小于最大的宽度的话
//            if (cuLineWidth + childUseWidth <= usedMaxWidth) {
//                itemLeft = left + cuLineWidth;
//                itemTop = cuLineTop;
//                Rect frame = allItemFrames.get(i);
//                if (frame == null) {
//                    frame = new Rect();
//                }
//                frame.set(itemLeft, itemTop, itemLeft + childWidth, itemTop + childHeight);
//                allItemFrames.put(i, frame);
//                cuLineWidth += childUseWidth;
//                maxHeightItem = Math.max(maxHeightItem, childUseHeight);
//                row.addViews(new Item(childUseHeight, childAt, frame));
//            } else {
//                //换行
//                formatAboveRow();
//                cuLineTop += maxHeightItem;
//                totalHeight += maxHeightItem;
//                itemTop = cuLineTop;
//                itemLeft = left;
//                Rect frame = allItemFrames.get(i);
//                if (frame == null) {
//                    frame = new Rect();
//                }
//                frame.set(itemLeft, itemTop, itemLeft + childWidth, itemTop + childHeight);
//                allItemFrames.put(i, frame);
//                cuLineWidth = childUseWidth;
//                maxHeightItem = childUseHeight;
//                row.addViews(new Item(childUseHeight, childAt, frame));
//            }
            row.setCuTop(cuLineTop);
            row.setMaxHeight(maxHeightItem);
            //不要忘了最后一行进行刷新下布局
            if (i == getItemCount() - 1) {
                formatAboveRow();
                totalHeight += maxHeightItem;
            }

        }
        totalHeight = Math.max(totalHeight, getVerticalSpace());
        fillLayout(recycler, state);
    }

    public int dip2px(float dpValue) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    //对出现在屏幕上的item进行展示，超出屏幕的item回收到缓存中
    private void fillLayout(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (state.isPreLayout() || getItemCount() == 0) { // 跳过preLayout，preLayout主要用于支持动画
            return;
        }

        // 当前scroll offset状态下的显示区域
        Rect displayFrame = new Rect(getPaddingLeft(), getPaddingTop() + verticalScrollOffset,
                getWidth() - getPaddingRight(), verticalScrollOffset + (getHeight() - getPaddingBottom()));

        //对所有的行信息进行遍历
        for (int j = 0; j < lineRows.size(); j++) {
            Row row = lineRows.get(j);
            float lineTop = row.cuTop;
            float lineBottom = lineTop + row.maxHeight;
            //如果该行在屏幕中，进行放置item
//            if (lineTop < displayFrame.bottom && displayFrame.top < lineBottom) {
            List<Item> views = row.views;
            for (int i = 0; i < views.size(); i++) {
                View scrap = views.get(i).view;
                measureChildWithMargins(scrap, 0, 0);
                addView(scrap);
                Rect frame = views.get(i).rect;
                //将这个item布局出来
                layoutDecoratedWithMargins(scrap,
                        frame.left,
                        frame.top - verticalScrollOffset,
                        frame.right,
                        frame.bottom - verticalScrollOffset);
            }
//            } else {
//                //将不在屏幕中的item放到缓存中
//                List<Item> views = row.views;
//                for (int i = 0; i < views.size(); i++) {
//                    View scrap = views.get(i).view;
//                    removeAndRecycleView(scrap, recycler);
//                }
//            }
        }
    }

    /**
     * 计算每一行没有居中的viewgroup，让居中显示
     */
    private void formatAboveRow() {
        List<Item> views = row.views;
        for (int i = 0; i < views.size(); i++) {
            Item item = views.get(i);
            View view = item.view;
            int position = getPosition(view);
            //如果该item的位置不在该行中间位置的话，进行重新放置
            if (allItemFrames.get(position).top < row.cuTop + (row.maxHeight - views.get(i).useHeight) / 2) {
                Rect frame = allItemFrames.get(position);
                if (frame == null) {
                    frame = new Rect();
                }
                frame.set(allItemFrames.get(position).left, (int) (row.cuTop + (row.maxHeight - views.get(i).useHeight) / 2),
                        allItemFrames.get(position).right, (int) (row.cuTop + (row.maxHeight - views.get(i).useHeight) / 2 + getDecoratedMeasuredHeight(view)));
                allItemFrames.put(position, frame);
                item.setRect(frame);
                views.set(i, item);
            }
        }
        row.views = views;
        lineRows.add(row);
        row = new Row();
    }

    /**
     * 竖直方向需要滑动的条件
     *
     * @return
     */
    @Override
    public boolean canScrollVertically() {
        return true;
    }

    //监听竖直方向滑动的偏移量
    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler,
                                  RecyclerView.State state) {

        Log.d("TAG", "totalHeight:" + totalHeight);
        //实际要滑动的距离
        int travel = dy;

        //如果滑动到最顶部
        if (verticalScrollOffset + dy < 0) {//限制滑动到顶部之后，不让继续向上滑动了
            travel = -verticalScrollOffset;//verticalScrollOffset=0
        } else if (verticalScrollOffset + dy > totalHeight - getVerticalSpace()) {//如果滑动到最底部
            travel = totalHeight - getVerticalSpace() - verticalScrollOffset;//verticalScrollOffset=totalHeight - getVerticalSpace()
        }

        //将竖直方向的偏移量+travel
        verticalScrollOffset += travel;

        // 平移容器内的item
        offsetChildrenVertical(-travel);
        fillLayout(recycler, state);
        return travel;
    }

    private int getVerticalSpace() {
        return self.getHeight() - self.getPaddingBottom() - self.getPaddingTop();
    }

    public int getHorizontalSpace() {
        return self.getWidth() - self.getPaddingLeft() - self.getPaddingRight();
    }

}