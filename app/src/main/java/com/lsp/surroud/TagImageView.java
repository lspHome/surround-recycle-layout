package com.lsp.surroud;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class TagImageView extends android.support.v7.widget.AppCompatImageView implements ITag {

    public int tagResource = 0;

    public TagImageView(Context context) {
        super(context);
    }

    public TagImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TagImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public String getOnlyTag() {
        return String.valueOf(tagResource);
    }
}
