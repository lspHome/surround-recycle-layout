package com.lsp.surroud;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.util.Log;
import android.view.View;

public class TranslateAnimation {
    private static final float DEFAULT_SCALE_FROM = .5f;
    private final float mFrom;

    private final String TAG = "TranslateAnimation";

    public TranslateAnimation() {
        this(DEFAULT_SCALE_FROM);
    }

    public TranslateAnimation(float from) {
        mFrom = from;
    }

    public Animator[] getAnimators(View view) {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
        PositionTag position = null;
        if (view.getTag() != null){
            position = (PositionTag) view.getTag();
        }
        if (position == null){
            position = new PositionTag(0,0,0,0);
        }
        //差值
        ObjectAnimator transX = ObjectAnimator.ofFloat(view, "translationX",   position.oldX - position.nowX , 0);
        ObjectAnimator transY = ObjectAnimator.ofFloat(view, "translationY", position.oldY - position.nowY , 0);
        return new ObjectAnimator[]{scaleX, scaleY,transX,transY};
    }

    public Animator[] getAnimators(View view,int translationX,int translationY) {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
        ObjectAnimator transX = ObjectAnimator.ofFloat(view, "translationX",   translationX , 0);
        ObjectAnimator transY = ObjectAnimator.ofFloat(view, "translationY", translationY, 0);
        return new ObjectAnimator[]{scaleX, scaleY,transX,transY};
    }

    public Animator[] getScaleAnimators(View view,int translationX,int translationY) {
//        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX",1f, mFrom );
//        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 1f, mFrom);
        ObjectAnimator transX = ObjectAnimator.ofFloat(view, "translationX",   translationX , 0);
        ObjectAnimator transY = ObjectAnimator.ofFloat(view, "translationY", translationY, 0);
//        scaleX, scaleY,
        return new ObjectAnimator[]{transX,transY};
    }
}
